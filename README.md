# Demo ui with OpenUI5

![App sample](docs/basic.png)

# Build

Inside the project, open terminal and run
`npm i`
to install project dependencies. Also remember to do this when you switch between tags.

# Local dev

To run the application, open your terminal and run

`npm start`
