//this is similar to AngularJs 1.x, we define a new Controller with MessageToast as depdendency
sap.ui.define(
  ["sap/ui/core/mvc/Controller", "sap/ui/core/routing/History"],
  function(Controller, History) {
    "use strict";
    //we return a new controller with ID 'com.acme.trainings.demoui.controller.App'
    return Controller.extend("com.acme.trainings.demoui.controller.Styling", {
      //called by framework when our ctrl is initialized
      onInit: function() {},

      onNavBack: function() {
        var oHistory = History.getInstance();
        var sPreviousHash = oHistory.getPreviousHash();

        if (sPreviousHash !== undefined) {
          window.history.go(-1);
        }
      }
    });
  }
);
