//this is similar to AngularJs 1.x, we define a new Controller with MessageToast as depdendency
sap.ui.define(
  ["sap/ui/core/mvc/Controller", "sap/ui/model/json/JSONModel"],
  function(Controller, JSONModel) {
    "use strict";
    //we return a new controller with ID 'com.acme.trainings.demoui.controller.App'
    return Controller.extend("com.acme.trainings.demoui.controller.RestData", {
      //called by framework when our ctrl is initialized
      onInit: function() {
        //create a new Omodel
        var modelFromRest = new JSONModel([]);
        //we use JS Fetch api to call backend
        fetch(
          "https://petstore.swagger.io/v2/pet/findByStatus?status=available"
        )
          //it is promise based, so after we got response, we map it to json
          .then(response => {
            return response.json();
          })
          //we can chain the promise callbacks
          .then(data => {
            console.log(data);
            //data is now body of the response - our pet list
            //update data of the model
            modelFromRest.setData(data);
          });

        this.getView().setModel(modelFromRest, "restData");
      },
      fetchData: function() {}
    });
  }
);
