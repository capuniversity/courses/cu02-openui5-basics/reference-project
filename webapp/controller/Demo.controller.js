//this is similar to AngularJs 1.x, we define a new Controller with MessageToast as depdendency
sap.ui.define(["sap/ui/core/mvc/Controller"], function(Controller) {
  "use strict";
  //we return a new controller with ID 'com.acme.trainings.demoui.controller.App'
  return Controller.extend("com.acme.trainings.demoui.controller.Demo", {
    //called by framework when our ctrl is initialized
    onInit: function() {}
  });
});
