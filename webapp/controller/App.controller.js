//this is similar to AngularJs 1.x, we define a new Controller with MessageToast as depdendency
sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/model/json/JSONModel"
  ],
  function(Controller, MessageToast, JSONModel) {
    "use strict";
    //we return a new controller with ID 'com.acme.trainings.demoui.controller.App'
    return Controller.extend("com.acme.trainings.demoui.controller.App", {});
  }
);
