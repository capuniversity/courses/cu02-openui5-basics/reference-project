//this is similar to AngularJs 1.x, we define a new Controller with MessageToast as depdendency
sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/model/json/JSONModel"
  ],
  function(Controller, MessageToast, JSONModel) {
    "use strict";
    //we return a new controller with ID 'com.acme.trainings.demoui.controller.BindingDemo'
    return Controller.extend(
      "com.acme.trainings.demoui.controller.BindingDemo",
      {
        //called by framework when our ctrl is initialized
        onInit: function() {
          // define a new model - a simple js/json object
          var myModelData = {
            greeting: {
              name: "Bella",
              greet: "Ciao"
            },
            status: "ok",
            list: [
              {
                id: 1,
                title: "Hammer"
              },
              {
                id: 2,
                title: "Screwdriver"
              },
              {
                id: 3,
                title: "Chainsaw"
              }
            ]
          };
          //define a new OpenUI model
          var jsonModel = new JSONModel(myModelData);
          //set it as default model to our view
          this.getView().setModel(jsonModel);
        },

        onShowHello: function() {
          // read msg from i18n model we defined in manifest(line 35)
          var oBundle = this.getView()
            //get model named 'commonBundle'
            .getModel("commonBundle")
            //and its resourceBundle
            .getResourceBundle();

          //define a var we pass to the message
          var name = "Developer";
          //get key from our common.properties file, and replace first param with the value of our name var
          var sMsg = oBundle.getText("helloMsg", [name]);

          // show message
          MessageToast.show(sMsg);
        },
        onShowModel: function() {
          //get model bound to view = the same model we defined in onInit,
          //from the model, give me value of property under model path /formData
          var inputValue = this.getView()
            .getModel()
            .getProperty("/formData");
          //display the value in a toast. The backticks and ${} is a Javascript string interpolation`
          MessageToast.show(`Input value is: ${inputValue}`);
        }
      }
    );
  }
);
