//this is similar to AngularJs 1.x, we define a new Controller with MessageToast as depdendency
sap.ui.define(
  ["sap/ui/core/mvc/Controller", "sap/ui/core/UIComponent"],
  function(Controller, UIComponent) {
    "use strict";
    //we return a new controller with ID 'com.acme.trainings.demoui.controller.App'
    return Controller.extend("com.acme.trainings.demoui.controller.Home", {
      //called by framework when our ctrl is initialized
      onInit: function() {},
      //we get instance of router with this
      getRouter: function() {
        return UIComponent.getRouterFor(this);
      },
      //click handlers for nav buttons
      onNavToDemo: function() {
        //navigate via router - using page.name attr from manifest
        this.getRouter().navTo("demoPage");
      },
      onNavToBinding: function() {
        this.getRouter().navTo("bindingPage");
      },
      onNavToStyling: function() {
        this.getRouter().navTo("stylingPage");
      },
      onNavToRest: function() {
        this.getRouter().navTo("restdataPage");
      }
    });
  }
);
