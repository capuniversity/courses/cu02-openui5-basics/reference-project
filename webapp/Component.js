sap.ui.define(
  ["sap/ui/core/UIComponent", "sap/ui/model/json/JSONModel"],
  function(UIComponent, JSONModel) {
    "use strict";
    return UIComponent.extend("com.acme.trainings.demoui.Component", {
      metadata: {
        manifest: "json"
      },
      init: function() {
        // call the init function of the parent
        UIComponent.prototype.init.apply(this, arguments);
        //here we would put any initialization logic, that we want to execute on our app startup

        // create the views based on the url/hash
        this.getRouter().initialize();
      }
    });
  }
);
